package com.mobiquityinc.packer.validator;

import com.mobiquityinc.packer.exception.ValidationException;
import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;

import java.util.List;

/**
 * Validates a container according to pre-defined business rules.
 */
public class ConstraintsValidator implements Validator {
    private static final int MAX_CONTAINER_WEIGHT = 100;
    private static final int MAX_ITEMS = 15;
    private static final double MAX_WEIGHT = 100;
    private static final double MAX_COST = 100;

    public void validate(final Container container) throws ValidationException {
        if (container.getCapacity() > MAX_CONTAINER_WEIGHT) {
            String msg = String.format("Package weight cannot exceed %d", MAX_CONTAINER_WEIGHT);
            throw new ValidationException(msg);
        }
        validateItems(container.getItems());
    }

    private void validateItems(final List<Item> items) throws ValidationException {
        if (items.size() > 15) {
            String msg = String.format("The number of items cannot exceed %d", MAX_ITEMS);
            throw new ValidationException(msg);
        }


        for (Item item : items) {
            validateItemWeightAndCost(item);
        }
    }

    private void validateItemWeightAndCost(final Item item) throws ValidationException {
        if (item.getWeight() > MAX_WEIGHT) {
            String msg = String.format("Item %d's weight cannot exceed %f", item.getIndex(), MAX_WEIGHT);
            throw new ValidationException(msg);
        }
        if (item.getCost() > MAX_COST) {
            String msg = String.format("Item %d's cost cannot exceed %f", item.getIndex(), MAX_COST);
            throw new ValidationException(msg);
        }
    }
}
