package com.mobiquityinc.packer.validator;

import com.mobiquityinc.packer.exception.ValidationException;
import com.mobiquityinc.packer.model.Container;

/**
 * Used to validate that the Container conforms to the business rules.
 * Multiple implementations can be created according to different business requirements.
 */
public interface Validator {
    void validate(final Container containers) throws ValidationException;
}
