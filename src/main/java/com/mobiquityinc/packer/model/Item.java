package com.mobiquityinc.packer.model;

import java.util.Objects;

/**
 * An Item with a index (id), weight and cost (value).
 */
public class Item {
    private final int index;
    private final double weight;
    private final double cost;

    public Item(final int index,
                final double weight,
                final double cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    public int getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return index == item.index &&
                Double.compare(item.weight, weight) == 0 &&
                Double.compare(item.cost, cost) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, weight, cost);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Item{");
        sb.append("index=").append(index);
        sb.append(", weight=").append(weight);
        sb.append(", cost=").append(cost);
        sb.append('}');
        return sb.toString();
    }
}
