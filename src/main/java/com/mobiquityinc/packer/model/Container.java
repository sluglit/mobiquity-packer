package com.mobiquityinc.packer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Container that store multiple {@link Item}s.
 * <i>Also known as a Package or Knapsack</i>
 */
public class Container {
    private final double capacity;
    private List<Item> items;

    public Container(final double capacity,
                     final List<Item> items) {
        this.capacity = capacity;
        this.items = items;
    }

    public double getCapacity() {
        return capacity;
    }


    public List<Item> getItems() {
        // Prevent returning null's
        if (items == null) {
            items = new ArrayList<>();
        }
        return items;
    }

    public void setItems(final List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Container container = (Container) o;
        return Double.compare(container.capacity, capacity) == 0 &&
                Objects.equals(items, container.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, items);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Container{");
        sb.append("capacity=").append(capacity);
        sb.append(", items=").append(items);
        sb.append('}');
        return sb.toString();
    }
}
