package com.mobiquityinc.packer.exception;

/**
 * The class {@code APIException} and its subclasses are a form of
 * {@code Exception} that indicates conditions that a reasonable
 * application might want to catch.
 */
public class APIException extends Exception {
    public APIException(final String msg) {
        this(msg, null);
    }

    public APIException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
