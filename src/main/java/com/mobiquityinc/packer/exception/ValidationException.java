package com.mobiquityinc.packer.exception;

/**
 * An exception that signals that a Validation exception has occurred. A
 * <code>ValidationException</code> object contains a <code>String</code>
 * that gives the reason for the exception.
 */
public class ValidationException extends APIException {
    public ValidationException(final String msg) {
        super(msg);
    }

    public ValidationException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
