package com.mobiquityinc.packer.exception;

/**
 * An exception that signals that a Parsing exception has occurred. A
 * <code>ParseException</code> object contains a <code>String</code>
 * that gives the reason for the exception.
 */
public class ParseException extends APIException {
    public ParseException(final String msg) {
        super(msg);
    }
}
