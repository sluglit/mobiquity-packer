package com.mobiquityinc.packer.utils;

public class StringUtils {
    public static boolean isBlank(final String str) {
        return str == null || str.trim().length() == 0;
    }
}
