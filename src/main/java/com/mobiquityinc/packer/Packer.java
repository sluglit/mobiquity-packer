package com.mobiquityinc.packer;

import com.mobiquityinc.packer.exception.APIException;
import com.mobiquityinc.packer.exception.ParseException;
import com.mobiquityinc.packer.exception.ValidationException;
import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.parser.Parser;
import com.mobiquityinc.packer.parser.RegexParser;
import com.mobiquityinc.packer.solver.Solver;
import com.mobiquityinc.packer.solver.branch.BranchAndBoundSolver;
import com.mobiquityinc.packer.utils.StringUtils;
import com.mobiquityinc.packer.validator.ConstraintsValidator;
import com.mobiquityinc.packer.validator.Validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Packer engine reads input from a file, calculates the best
 */
public class Packer {
    /**
     * Validates the {@link Container}
     */
    private static Validator validator = new ConstraintsValidator();
    /**
     * Parses text input into {@link Container}s
     */
    private static Parser parser = new RegexParser();

    /**
     * Knapsack Solver implementation
     */
    private static Solver solver = new BranchAndBoundSolver();

    /**
     * Parser parses each line of text from the input file.
     *
     * @return Parser instance.
     */
    public static Parser getParser() {
        return parser;
    }

    /**
     * Solver implementation to find the best fit of the available packages.
     *
     * @return Solver implementation.
     */
    public static Solver getSolver() {
        return solver;
    }

    /**
     * Sets the parser used to parse each line of text from the input file.
     *
     * @param parser {@link Parser} implementation.
     */
    public static void setParser(Parser parser) {
        Packer.parser = parser;
    }

    /**
     * Sets the Solver implementation used to figure out which packages to select from the list of available packages.
     *
     * @param solver
     */
    public static void setSolver(Solver solver) {
        Packer.solver = solver;
    }

    /**
     * Reads the <code>filename</code> input and selects
     *
     * @param filename Input file
     * @return Comma separated list of selected package id's.
     * @throws APIException
     */
    public static String pack(final String filename) throws APIException {
        try {
            final File file = new File(filename);
            return processFile(file.toPath());
        } catch (NoSuchFileException e) {
            final String msg = String.format("File not found '%s'", filename);
            throw new APIException(msg);
        } catch (IOException e) {
            final String msg = String.format("Error processing file '%s': %s", filename, e.getMessage());
            throw new APIException(msg);
        }
    }

    /**
     * Processes the input file.
     * <i><Reads and processes the input file line by line to reduce the memory overhead./i>
     */
    private static String processFile(final Path path) throws IOException, ParseException, ValidationException {
        final StringBuilder result = new StringBuilder();

        try (final BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                final String lineResult = processLine(line);
                if (result.length() > 0) {
                    result.append(System.lineSeparator());
                }

                result.append(lineResult);
            }
        }

        return result.toString();
    }


    /**
     * Responsible for delegating processing to the {@link Parser}, {@link Validator} and {@link Solver} and finding the best solution.
     */
    private static String processLine(final String line) throws ParseException, ValidationException {
        try {
            Container solution = null;
            if (!StringUtils.isBlank(line)) {
                final Container container = parser.parse(line);
                validator.validate(container);
                solution = solver.solve(container.getCapacity(), container.getItems());
            }
            return print(solution);
        } catch (ValidationException e) {
            String msg = String.format("Error validating line %s: %s", line, e.getMessage());
            throw new ValidationException(msg, e);
        }
    }

    /**
     * Build the output string in a constant way, by sorting the index values.
     */
    private static String print(final Container container) {
        if (container != null) {
            final List<Item> items = container.getItems();
            if (!items.isEmpty()) {
                return container.getItems()
                        .stream()
                        .map(Item::getIndex)
                        .map(String::valueOf)
                        .sorted()
                        .collect(Collectors.joining(","));
            }
        }
        return "-";
    }
}
