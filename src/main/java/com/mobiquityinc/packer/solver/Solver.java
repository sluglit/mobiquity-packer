package com.mobiquityinc.packer.solver;

import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;

import java.util.List;

/**
 * Interface to allow the implementation of different algorithms to determine the optimal solution for packing
 * a number of items into a container.
 */
public interface Solver {
    Container solve(final double capacity, final List<Item> items);
}
