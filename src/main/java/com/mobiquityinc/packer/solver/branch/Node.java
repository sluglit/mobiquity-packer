package com.mobiquityinc.packer.solver.branch;

import com.mobiquityinc.packer.model.Item;

import java.util.ArrayList;
import java.util.List;

class Node implements Comparable<Node> {
    /**
     * Level represents the current item's index based on the list of items provided.
     */
    private int level;
    /**
     * A ranking value used to calculate the potential of the next items in the list.
     */
    private double bound;
    /**
     * Cumulative cost of all eligible items
     */
    private double totalCost;
    /**
     * Cumulative weight of all eligible items
     */
    private double totalWeight;

    /**
     * List of eligible items
     */
    private List<Item> eligible = new ArrayList<>();

    /**
     * Capacity of the container
     */
    private double capacity;
    /**
     * Comprehensive list of all items
     */
    private List<Item> items;

    public Node(final double capacity,
                final List<Item> items) {
        this.capacity = capacity;
        this.items = items;
        this.bound = calculateBound();
    }

    /**
     * Creates a child {@link Node} instance based on the parent node and sets the current level to the next {@link Item} in the list of items.
     *
     * @param parent Parent node to base this node on.
     */
    public Node(Node parent) {
        this.capacity = parent.getCapacity();
        this.items = parent.getItems();

        this.level = parent.level + 1;
        this.eligible.addAll(parent.eligible);
        this.bound = parent.bound;
        this.totalCost = parent.totalCost;
        this.totalWeight = parent.totalWeight;

        this.bound = calculateBound();
    }

    /**
     * Sort by bound
     */
    public int compareTo(Node other) {
        return Double.compare(other.bound, bound);
    }

    /**
     * Calculates the cumulative bound value of the current item and subsequent items until the sum of the weight of the items exceeds the total capacity.
     *
     * @return Bound value
     */
    private double calculateBound() {
        int i = level;
        double weight = totalWeight;
        double result = totalCost;
        Item item;
        do {
            item = items.get(i);
            if (weight + item.getWeight() > capacity) {
                break;
            }
            weight += item.getWeight();
            result += item.getCost();
            i++;
        } while (i < items.size());
        result += (capacity - weight) * (item.getCost() / item.getWeight());
        return result;
    }

    /**
     * Add an {@link Item} to the list of eligible items
     *
     * @param item
     */
    public void addItem(final Item item) {
        this.eligible.add(item);
        this.totalWeight += item.getWeight();
        this.totalCost += item.getCost();
        this.bound = calculateBound();
    }

    /**
     * Current position based on the index of all the items
     *
     * @return
     */
    public int getLevel() {
        return level;
    }

    /**
     * Potential bound of the current item and subsequent eligible items.
     *
     * @return
     */
    public double getBound() {
        return bound;
    }

    /**
     * Total cost of all eligible items.
     *
     * @return
     */
    public double getTotalCost() {
        return totalCost;
    }


    /**
     * Total weight of all eligible items.
     *
     * @return
     */
    public double getTotalWeight() {
        return totalWeight;
    }

    /**
     * List of eligible items
     *
     * @return
     */
    public List<Item> getEligible() {
        return eligible;
    }

    /**
     * Capacity of the container
     *
     * @return
     */
    public double getCapacity() {
        return capacity;
    }

    /**
     * List of all {@link Item}'s
     *
     * @return
     */
    public List<Item> getItems() {
        return items;
    }
}
