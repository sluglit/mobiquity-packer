package com.mobiquityinc.packer.solver.branch;

import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.solver.SortedSolver;

import java.util.List;
import java.util.PriorityQueue;

/**
 * Uses the Branch and Bound approach to solve the Knapsack problem by sorting the items based on their ratio, then traversing each item in a graph approach.
 * Items that exceed the capacity bounds are skipped until an optimal solution is found.
 * <p>Time Complexity: Approximately O(NlogN)</p>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Branch_and_bound">Branch and bound</a>
 */
public class BranchAndBoundSolver extends SortedSolver {

    @Override
    public Container solveInternal(final double capacity, final List<Item> items) {
        Node result = new Node(capacity, items);
        final Node root = new Node(capacity, items);


        // Priority queue takes orders the list of Nodes, sorts and offers the Nodes based on their Comparator implementation.
        final PriorityQueue<Node> queue = new PriorityQueue<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            final Node node = queue.poll();

            if (node.getBound() > result.getTotalCost()
                    && node.getLevel() < items.size() - 1) {
                final Node childIncluding = new Node(node);
                final Item item = items.get(node.getLevel());
                childIncluding.addItem(item);

                if (childIncluding.getTotalWeight() <= capacity) {
                    if (childIncluding.getTotalCost() > result.getTotalCost()) {
                        result = childIncluding;
                    }
                    if (childIncluding.getBound() > result.getTotalCost()) {
                        queue.offer(childIncluding);
                    }
                }

                final Node childExcluding = new Node(node);
                if (childExcluding.getBound() > result.getTotalCost()) {
                    queue.offer(childExcluding);
                }
            }
        }

        // Create a Container instance with the list of eligible Items
        return new Container(capacity, result.getEligible());
    }
}


