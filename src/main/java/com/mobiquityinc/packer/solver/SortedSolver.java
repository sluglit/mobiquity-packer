package com.mobiquityinc.packer.solver;

import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Sorts the provided items based on Cost/Weight ratio before handing them over to the solver implementation class.
 */
public abstract class SortedSolver implements Solver {

    private static List<Item> sortItemsByRatio(final List<Item> items) {
        return items.stream()
                .sorted((o1, o2) -> {
                    double ratio1 = o1.getCost() / o1.getWeight();
                    double ratio2 = o2.getCost() / o2.getWeight();
                    return Double.compare(ratio1, ratio2) * -1;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Container solve(final double capacity, List<Item> items) {
        List<Item> sorted = sortItemsByRatio(items);
        return solveInternal(capacity, sorted);
    }

    protected abstract Container solveInternal(final double capacity, final List<Item> items);

}
