package com.mobiquityinc.packer.solver.dynamic;

import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.solver.SortedSolver;

import java.util.ArrayList;
import java.util.List;

/**
 * 0-1 knapsack solver using a dynamic implementation.
 *
 * <p>Time Complexity: O(nW)</p>
 * <i>Note: The main reason this implementation is here was so that I had something to compare the {@link com.mobiquityinc.packer.solver.branch.BranchAndBoundSolver} to.</i>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Knapsack_problem">Knapsack problem</a>
 */
public class DynamicSolver extends SortedSolver {

    @Override
    public Container solveInternal(final double capacity, final List<Item> items) {
        final Item[] itemArr = items.toArray(new Item[items.size()]);
        // Normalize weight to units
        final int normalizedCapacity = normalizedouble(capacity);

        int[][] matrix = solve(normalizedCapacity, itemArr);

        return buildResult(itemArr, matrix, normalizedCapacity);
    }

    private int[][] solve(final int capacity, final Item[] items) {
        final int itemCount = items.length;
        // we use a matrix to store the max value at each n-th item
        final int[][] matrix = new int[itemCount + 1][capacity + 1];

        // Initialize first line to 0
        for (int i = 0; i <= capacity; i++) {
            matrix[0][i] = 0;
        }

        // Iterate over the items starting at 1
        for (int i = 1; i <= itemCount; i++) {
            // Iterate on each capacity step
            for (int j = 0; j <= capacity; j++) {
                final Item currentItem = items[i - 1];
                int currentWeight = normalizedouble(currentItem.getWeight());

                if (currentWeight > j) {
                    matrix[i][j] = matrix[i - 1][j];
                } else {
                    int currentCost = normalizedouble(currentItem.getCost());
                    // Calculate the highest value comparing previous value's cost and the current values cost
                    int value = Math.max(
                            matrix[i - 1][j],
                            matrix[i - 1][j - currentWeight] + currentCost
                    );
                    matrix[i][j] = value;
                }
            }
        }

        return matrix;
    }

    private Container buildResult(final Item[] items,
                                  final int[][] matrix,
                                  final int capacity) {
        int itemCount = items.length;
        final List<Item> resultItems = new ArrayList<>();
        int remainingCost = matrix[itemCount][capacity];
        int remainingCapacity = capacity;

        for (int i = itemCount; i > 0 && remainingCost > 0; i--) {
            if (remainingCost != matrix[i - 1][remainingCapacity]) {
                Item currentItem = items[i - 1];
                resultItems.add(currentItem);
                // we remove items value and weight
                remainingCost -= currentItem.getCost() * 100;
                remainingCapacity -= currentItem.getWeight() * 100;
            }
        }

        return new Container(capacity, resultItems);
    }

    private int normalizedouble(double val) {
        return (int) (val * 100);
    }
}
