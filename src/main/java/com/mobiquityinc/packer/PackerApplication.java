package com.mobiquityinc.packer;

import com.mobiquityinc.packer.exception.APIException;

/**
 * Main application CLI entry point
 */
public class PackerApplication {
    public static void main(final String[] args) {
        if (args.length != 1) {
            System.out.println("Error: Missing file argument");
            System.out.println("Usage:");
            System.out.println("\t# java -jar packer.jar <input_file>");
            System.exit(-1);
            return;
        }


        try {
            String answer = Packer.pack(args[0]);
            System.out.println(answer);
            System.exit(0);
        } catch (APIException e) {
            System.out.println("Error:");
            System.out.print("\t");
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }


}

