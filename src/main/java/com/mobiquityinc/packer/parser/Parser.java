package com.mobiquityinc.packer.parser;

import com.mobiquityinc.packer.exception.ParseException;
import com.mobiquityinc.packer.model.Container;

/**
 * Parses a line of text into a Container instance.
 */
public interface Parser {
    Container parse(final String line) throws ParseException;
}
