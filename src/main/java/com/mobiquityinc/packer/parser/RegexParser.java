package com.mobiquityinc.packer.parser;

import com.mobiquityinc.packer.exception.ParseException;
import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses each input line string into a Container instance
 */
public class RegexParser implements Parser {
    /**
     * Pattern to group by package size and items
     * e.g.: (package size), (items string)
     */
    public static final Pattern PATTERN_PACKAGE = Pattern.compile("(\\d+)\\s*:\\s*(.*)");
    /**
     * Pattern to split the list of Items, into individual Itemss
     * e.g.: [(index,weight,cost), (index,weight,cost), ...]
     */
    public static final Pattern PATTERN_ITEMS = Pattern.compile("(\\(\\d+,[\\d\\.]+,€\\d+\\))");

    /**
     * Pattern to split a item, into index, weight and cost
     */
    public static final Pattern PATTERN_ITEM = Pattern.compile("\\((\\d+),([\\d\\.]+),€([\\d\\.]+)\\)");

    public Container parse(final String knapsackStr) throws ParseException {
        if (StringUtils.isBlank(knapsackStr)) {
            throw new ParseException("Input in required");
        }
        Container result = processPackage(knapsackStr);
        return result;
    }

    /**
     * Processes a string representing a package with size and items.
     */
    private Container processPackage(final String packageStr) throws ParseException {
        final Matcher matcher = PATTERN_PACKAGE.matcher(packageStr);
        if (matcher.matches()) {
            // Retrieve values
            final int maxWeight = Integer.valueOf(matcher.group(1));
            final String thingsStr = matcher.group(2);

            // Parse Items string
            final List<Item> items = parseItems(thingsStr);

            return new Container(maxWeight, items);
        } else {
            final String msg = String.format("Invalid input format: %s", packageStr);
            throw new ParseException(msg);
        }
    }

    /**
     * Parses the items portion of the input string
     */
    private List<Item> parseItems(final String itemsStr) throws ParseException {
        final List<Item> result = new ArrayList<>();
        final Matcher matcher = PATTERN_ITEMS.matcher(itemsStr);
        while (matcher.find()) {
            // Retrieve Item string
            final String thingStr = matcher.group(1);
            // Parse Item string
            final Item item = parseItem(thingStr);

            result.add(item);
        }

        if (result.isEmpty() && !StringUtils.isBlank(itemsStr)) {
            final String msg = String.format("Unexpected item format: %s", itemsStr);
            throw new ParseException(msg);
        }
        return result;
    }

    /**
     * Parses a single item portion of the package string.
     */
    private Item parseItem(final String itemStr) {
        final Matcher matcher = PATTERN_ITEM.matcher(itemStr);
        //This should always pass based on the previous PATTERN_ITEMS match
        matcher.matches();

        int groupIdx = 1;
        // Retrieve Item values
        int index = Integer.parseInt(matcher.group(groupIdx++));
        double weight = Double.parseDouble(matcher.group(groupIdx++));
        double cost = Double.parseDouble(matcher.group(groupIdx++));

        // Create the Item instance
        return new Item(index, weight, cost);

    }
}
