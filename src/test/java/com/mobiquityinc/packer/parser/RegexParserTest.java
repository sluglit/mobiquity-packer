package com.mobiquityinc.packer.parser;

import com.mobiquityinc.packer.exception.ParseException;
import com.mobiquityinc.packer.model.Container;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegexParserTest {
    private Parser parser = new RegexParser();


    @Test
    public void passParse() throws ParseException {
        final String data = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        final Container container = parser.parse(data);

        assertNotNull(container);
        assertEquals("Max package weight should be 81", 81, container.getCapacity(), 0.01);
        assertEquals("There should be 6 items in this package", 6, container.getItems().size());
    }

    @Test
    public void passParse_MissingWhitespace() throws ParseException {
        final String data = "81:(1,53.38,€45)(2,88.62,€98)(3,78.48,€3)(4,72.30,€76)(5,30.18,€9)(6,46.34,€48)";
        final Container pkg = parser.parse(data);

        assertNotNull(pkg);
        assertEquals("Max package weight should be 81", 81, pkg.getCapacity(), 0.01);
        assertEquals("There should be 6 Items in this package", 6, pkg.getItems().size());
    }

    @Test
    public void passParse_NoThings() throws ParseException {
        final String data = "81 :";
        final Container pkg = parser.parse(data);

        assertNotNull(pkg);
        assertEquals("Max package weight should be 81", 81, pkg.getCapacity(), 0.01);
        assertEquals("There should be 0 things in this package", 0, pkg.getItems().size());
    }


    @Test(expected = ParseException.class)
    public void failParse_BadCapacity() throws ParseException {
        final String data = "a : (1,25,€45)";
        final Container pkg = parser.parse(data);

        assertNotNull(pkg);
        assertEquals("Max package weight should be 81", 81, pkg.getCapacity(), 0.01);
        assertEquals("There should be 0 things in this package", 0, pkg.getItems().size());
    }

    @Test(expected = ParseException.class)
    public void failParse_NoInput() throws ParseException {
        final String data = "";
        parser.parse(data);

        fail("Should throw ParseException");
    }

    @Test(expected = ParseException.class)
    public void failParse_InvalidThingFormat() throws ParseException {
        final String data = "100: {a,b,c}";
        parser.parse(data);

        fail("Should throw ParseException");
    }
}