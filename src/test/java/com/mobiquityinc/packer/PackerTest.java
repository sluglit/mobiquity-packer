package com.mobiquityinc.packer;


import com.mobiquityinc.packer.exception.APIException;
import com.mobiquityinc.packer.solver.branch.BranchAndBoundSolver;
import com.mobiquityinc.packer.solver.dynamic.DynamicSolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Packer JUnit Tests
 */
public class PackerTest {
    @Before
    public void setUp() {
        // Reset the solver implementation for each test to BranchAndBoundSolver
        Packer.setSolver(new BranchAndBoundSolver());
    }

    @Test
    public void passPack_BranchAndBound() throws APIException, URISyntaxException {
        Packer.setSolver(new BranchAndBoundSolver());

        StringBuilder expected = new StringBuilder()
                .append("4")
                .append(System.lineSeparator())
                .append("-")
                .append(System.lineSeparator())
                .append("2,7")
                .append(System.lineSeparator())
                .append("8,9");

        String path = classPathToPath("input.txt");
        String result = Packer.pack(path);
        Assert.assertEquals(expected.toString(), result);
    }

    @Test
    public void passPack_Dynamic() throws APIException, URISyntaxException {
        Packer.setSolver(new DynamicSolver());

        StringBuilder expected = new StringBuilder()
                .append("4")
                .append(System.lineSeparator())
                .append("-")
                .append(System.lineSeparator())
                .append("2,7")
                .append(System.lineSeparator())
                .append("8,9");

        String path = classPathToPath("input.txt");
        String result = Packer.pack(path);
        Assert.assertEquals(expected.toString(), result);
    }

    @Test(expected = APIException.class)
    public void failPack_BadFormat() throws URISyntaxException, APIException {
        String path = classPathToPath("badformat.txt");
        Packer.pack(path);

        Assert.fail("Should throw an APIException");
    }

    @Test(expected = APIException.class)
    public void failPack_FileNotFound() throws APIException {
        try {
            Packer.pack("notfound.dat");
            Assert.fail("Should throw an APIException");
        } catch (APIException e) {
            Assert.assertEquals("File not found 'notfound.dat'", e.getMessage());
            throw e;
        }
    }

    private String classPathToPath(final String classpath) throws URISyntaxException {
        URL url = ClassLoader.getSystemResource(classpath);
        File file = new File(url.toURI());
        return file.getAbsolutePath();
    }
}