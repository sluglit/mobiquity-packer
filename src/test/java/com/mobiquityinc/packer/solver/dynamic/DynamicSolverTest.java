package com.mobiquityinc.packer.solver.dynamic;

import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.solver.Solver;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DynamicSolverTest {
    private Solver solver = new DynamicSolver();

    @Test
    public void passSolve_1() {
        //81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)

        double capacity = 81;
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, 53.38, 45));
        items.add(new Item(2, 88.62, 98));
        items.add(new Item(3, 78.48, 3));
        items.add(new Item(4, 72.30, 76));
        items.add(new Item(5, 30.18, 9));
        items.add(new Item(6, 46.34, 48));


        Container solution = solver.solve(capacity, items);

        List<Item> expected = new ArrayList<>();
        expected.add(new Item(4, 72.30, 76));

        Assert.assertNotNull(solution);
        checkResult(expected, solution);
    }

    @Test
    public void passSolve_2() {
        //8 : (1,15.3,€34)

        double capacity = 8;
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, 15.3, 34));

        Container solution = solver.solve(capacity, items);

        List<Item> expected = new ArrayList<>();

        Assert.assertNotNull(solution);
        checkResult(expected, solution);
    }

    @Test
    public void passSolve_3() {
        //75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)

        double capacity = 75;
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, 85.31, 29));
        items.add(new Item(2, 14.55, 74));
        items.add(new Item(3, 3.98, 16));
        items.add(new Item(4, 26.24, 55));
        items.add(new Item(5, 63.69, 52));
        items.add(new Item(6, 76.25, 75));
        items.add(new Item(7, 60.02, 74));
        items.add(new Item(8, 93.18, 35));
        items.add(new Item(9, 89.95, 78));

        Container solution = solver.solve(capacity, items);

        List<Item> expected = new ArrayList<>();
        expected.add(new Item(2, 14.55, 74));
        expected.add(new Item(7, 60.02, 74));

        Assert.assertNotNull(solution);
        checkResult(expected, solution);
    }

    @Test
    public void passSolve_4() {
        //56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)
        double capacity = 56;
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, 90.72, 13));
        items.add(new Item(2, 33.80, 40));
        items.add(new Item(3, 43.15, 10));
        items.add(new Item(4, 37.97, 16));
        items.add(new Item(5, 46.81, 36));
        items.add(new Item(6, 48.77, 79));
        items.add(new Item(7, 81.80, 45));
        items.add(new Item(8, 19.36, 79));
        items.add(new Item(9, 6.76, 64));


        Container solution = solver.solve(capacity, items);

        List<Item> expected = new ArrayList<>();
        expected.add(new Item(8, 19.36, 79));
        expected.add(new Item(9, 6.76, 64));

        Assert.assertNotNull(solution);
        checkResult(expected, solution);
    }

    private void checkResult(final List<Item> expected, final Container solution) {
        float totalWeight = 0;
        Assert.assertEquals(expected.size(), solution.getItems().size());
        for (Item expectedItem : expected) {
            totalWeight += expectedItem.getWeight();
            Assert.assertTrue(solution.getItems().contains(expectedItem));
        }
        Assert.assertTrue(totalWeight < solution.getCapacity());
    }
}