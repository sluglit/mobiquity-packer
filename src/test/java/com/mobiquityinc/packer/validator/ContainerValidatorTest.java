package com.mobiquityinc.packer.validator;

import com.mobiquityinc.packer.exception.ValidationException;
import com.mobiquityinc.packer.model.Container;
import com.mobiquityinc.packer.model.Item;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContainerValidatorTest {
    private Validator validator = new ConstraintsValidator();

    @Test
    public void passValidate() throws ValidationException {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            Item item = new Item(i, i, i);
            items.add(item);
        }
        Container container = new Container(10, items);
        validator.validate(container);
    }

    @Test(expected = ValidationException.class)
    public void failValidate_Weight() throws ValidationException {
        Item item = new Item(1, 200, 10);
        Container container = new Container(10, Arrays.asList(item));
        validator.validate(container);
    }

    @Test(expected = ValidationException.class)
    public void failValidate_Cost() throws ValidationException {
        Item item = new Item(1, 10, 200);
        Container container = new Container(10, Arrays.asList(item));
        validator.validate(container);
    }

    @Test(expected = ValidationException.class)
    public void failValidate_ItemCount() throws ValidationException {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            Item item = new Item(i, i, i);
            items.add(item);
        }
        Container container = new Container(10, items);
        validator.validate(container);
    }

    @Test(expected = ValidationException.class)
    public void failValidate_MaxWeight() throws ValidationException {
        List<Item> items = new ArrayList<>();
        Container container = new Container(200, items);
        validator.validate(container);
    }

}