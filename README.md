#Mobiquity Inc

##Assignment: Packer Challenge

###Documentation
Documentation can be found in the `docs` directory 



###Building the project
```
$ ./mvnw clean test install
```

##Running the application
```
$ java -jar target/packer.jar <input_file>
```


    
